package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    List<Task> findALL();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();
}
