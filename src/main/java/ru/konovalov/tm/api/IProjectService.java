package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

    Object add(String name, String description);
}
