package ru.konovalov.tm.api;

public interface ITaskController {
    void showList();

    void create();

    void clear();
}
