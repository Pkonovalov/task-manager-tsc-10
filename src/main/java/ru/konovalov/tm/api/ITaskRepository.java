package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    List<Task> findALL();

    void add(Task task);

    void remove(Task task);

    void clear();
}
