package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
