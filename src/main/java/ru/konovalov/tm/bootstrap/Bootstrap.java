package ru.konovalov.tm.bootstrap;

import ru.konovalov.tm.api.*;
import ru.konovalov.tm.constants.ArgumentConst;
import ru.konovalov.tm.constants.TerminalConst;
import ru.konovalov.tm.controller.CommandController;
import ru.konovalov.tm.controller.ProjectController;
import ru.konovalov.tm.controller.TaskController;
import ru.konovalov.tm.repository.CommandRepository;
import ru.konovalov.tm.repository.ProjectRepository;
import ru.konovalov.tm.repository.TaskRepository;
import ru.konovalov.tm.service.CommandService;
import ru.konovalov.tm.service.ProjectService;
import ru.konovalov.tm.service.TaskService;
import ru.konovalov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final  IProjectService projectService = new ProjectService(projectRepository) ;

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args){
        System.out.println("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public void showIncorrectArgument() {
        System.out.println("Error! Your argument is wrong! Try again it correctly!");
    }

    public void showIncorrectCommand() {
        System.out.println("Error! Your command is wrong! Try again it correctly!");
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }
}
