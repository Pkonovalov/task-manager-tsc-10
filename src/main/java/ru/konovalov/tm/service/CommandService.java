package ru.konovalov.tm.service;

import ru.konovalov.tm.api.ICommandRepository;
import ru.konovalov.tm.api.ICommandService;
import ru.konovalov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;
    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
