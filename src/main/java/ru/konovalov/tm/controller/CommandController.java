package ru.konovalov.tm.controller;

import ru.konovalov.tm.api.ICommandController;
import ru.konovalov.tm.api.ICommandService;
import ru.konovalov.tm.model.Command;
import ru.konovalov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Petr Konovalov");
        System.out.println("E -MAIL: pkonjob@yandex.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command.getName());
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(command);
        }
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory (bytes): " + NumberUtil.formatBytes(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory (bytes): " + maxMemoryValue);
        final long memoryAvailable = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM (bytes): " + NumberUtil.formatBytes(memoryAvailable));
        final long usedMemory = memoryAvailable - freeMemory;
        System.out.println("User memory by JVM (bytes): " + NumberUtil.formatBytes(usedMemory));
        System.out.println("[OK]");
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
