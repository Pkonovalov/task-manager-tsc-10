package ru.konovalov.tm;

import ru.konovalov.tm.bootstrap.Bootstrap;
public class  App {

    public static void main(String[] args) {
       final Bootstrap bootstrap = new Bootstrap();
       bootstrap.run(args);
    }
}